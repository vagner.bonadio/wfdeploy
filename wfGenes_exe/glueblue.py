
import numpy as np
import time

def callscript(*args, **kwargs):
    cmd = kwargs['command']
    if 'argument' in kwargs.keys():
        cmd = cmd + " " + str(kwargs['argument'])
    os.system(cmd)
    return 'Done!!! '


def printrun(*args):
    result = []
    for arg in args:
        if isinstance(arg, list) and len(arg) == 1:
            result.append(arg[0])
        else:
            result.append(arg)
    if len(result) == 1:
        result = result[0]
    print(result)
    return result


def MERGE(**kwargs):
    dic_merged = {}
    for key, value in kwargs.items():
        if isinstance(value, np.number):
            value = float(value)
        else:
            value = value
    return (kwargs)   


def flat_tuple(lazy_tuple, return_number):
    flat_list = []
    for i in range(return_number):
        flat_list.append([])
    for i in range(return_number):       
        for j in range(len(lazy_tuple)):    
            for item in lazy_tuple[j][i]:  
                flat_list[return_number- (i + 1)].append(item)
    return flat_list


def flat_list(lazy_list):
    flat_list = []
    for list in lazy_list:
            for item in list:
                flat_list.append(item)
    return flat_list



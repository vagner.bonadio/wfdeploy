#!/bin/bash/ -ex
#module purge
conda activate myenv
#module load chem/turbomole
#module load chem/vasp/
#export VASP_COMMAND="$DO_PARALLEL $VASPMPI"
#export ASE_VASP_VDW=$VASP_HOME/bin
export PYTHONPATH=/home/hk-project-test-sdlmat/th7356/work/gitlab/wfgenes/intro_examples/multihith/lib:$PYTHONPATH
export PYTHONPATH=/home/hk-project-test-sdlmat/th7356/work/gitlab/wfgenes/wfGenes_exe/:$PYTHONPATH
export PYTHONPATH=/home/hk-project-test-sdlmat/th7356/work/gitlab/wfgenes/intro_examples/foreach_sample/lib/:$PYTHONPATH
export PYTHONPATH=/home/hk-project-test-sdlmat/th7356/work/gitlab/wfgenes/intro_examples/rgg/lib:$PYTHONPATH
export PYTHONPATH=/home/hk-project-test-sdlmat/th7356/work/gitlab/wfgenes/intro_examples/simple_workflow/lib:$PYTHONPATH
export PYTHONPATH=/home/hk-project-test-sdlmat/th7356/work/gitlab/wfgenes/intro_examples/rgg/wfGenes_output/node_1001_1105/Dask/:$PYTHONPATH
export PYTHONPATH=/home/hk-project-test-sdlmat/th7356/work/gitlab/wfgenes/intro_examples/rgg/wfGenes_output/node_1001_1105/Parsl/:$PYTHONPATH
export PYTHONPATH=/home/hk-project-test-sdlmat/th7356/work/gitlab/wfgenes/intro_examples/rgg/wfGenes_output/node_501_592/Dask/:$PYTHONPATH
export PYTHONPATH=/home/hk-project-test-sdlmat/th7356/work/gitlab/wfgenes/intro_examples/rgg/wfGenes_output/node_501_592/Parsl/:$PYTHONPATH




#unset I_MPI_HYDRA_BOOTSTRAP I_MPI_HYDRA_RMK I_MPI_HYDRA_BRANCH_COUNT
#export I_MPI_HYDRA_BOOTSTRAP=ssh


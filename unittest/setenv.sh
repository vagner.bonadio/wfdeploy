#!/bin/bash/ -ex
module purge
conda activate myenv
module load chem/vasp/
export VASP_COMMAND="$DO_PARALLEL $VASPMPI"
export ASE_VASP_VDW=$VASP_HOME/bin
export PYTHONPATH="/pfs/data5/home/kit/scc/th7356/work/GITLAB/multihith/lib"
unset I_MPI_HYDRA_BOOTSTRAP I_MPI_HYDRA_RMK I_MPI_HYDRA_BRANCH_COUNT
export I_MPI_HYDRA_BOOTSTRAP=ssh


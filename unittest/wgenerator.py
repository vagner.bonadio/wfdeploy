
""" wfGenes: Automatic workflow generator."""


__author__ = 'Mehdi Roozmeh'
__email__ = 'mehdi.roozmeh@kit.edu'
__copyright__ = 'Copyright 2020, Karlsruhe Institute of Technology'


import os
import os.path
from copy import deepcopy
from collections import OrderedDict
import shutil
import argparse
import json
import xmlschema
import yaml
import fireworks_schema


def duplicate_finder(target_list):
    """ Detect duplicates in multidimensional list  """
    for y in range(len(target_list)):
        for x in range(len(target_list[y])):
            for i in range(y, len(target_list) - 1):
                for j in range(len(target_list[i + 1])):
                    if target_list[y][x] != target_list[i + 1][j]:
                        pass
                    else:
                        target_list[i + 1][j] = 'duplicate'
    return target_list


def dependency_check(input_list, output_list):
    """ Check dependency between different subroutine in one routine """
    for y in range(len(input_list) - 1, -1, -1):
        for x in range(len(input_list[y])):
            for i in range(len(output_list) - 1):
                for j in range(len(output_list[i])):
                    if input_list[y][x] != output_list[i][j]:
                        pass
                    else:
                        input_list[y][x] = 'inner_dependent'
    return input_list


def link_check(input_link, input_list, output_list, routine_id):
    """ Check dependency between different routines"""
    for y in range(len(input_list) - 1, -1, -1):
        for x in range(len(input_list[y])):
            for i in range(len(output_list) - 1, -1, -1):
                for j in range(len(output_list[i])):
                    if input_list[y][x] != output_list[i][j]:
                        pass
                    else:
                        input_link[y][x] = [str(routine_id), i, j]
    return input_link


def extractlist(lst):
    """ Return list of the list """
    return [[el] for el in lst]


parser = argparse.ArgumentParser()
parser.add_argument(
    '--workflowconfig',
    default='workflow.yaml',
    help='workflowconfig file contains data of each'
    ' routine in yaml format. i.e.'
    ' Input/Outputfile, modules and arguments list, the default is workflow.yaml.'
    ' For more information check README file.')

parser.add_argument('--inputpath', default='',
                    help='Set input directory of WMS.'
                    ' It is not necessary for all WMS,'
                    ' please refer to related manual in repository.')


parser.add_argument('--outputpath', default='',
                    help='Place the output into outputpath (optional).')


parser.add_argument('--wms', default='all',
                    help='Choose specific workflow management system.'
                    'The default behavior produces all supported WMSs.')


args = parser.parse_args()


if __name__ == '__main__':

    # List declaration
    input_stream = open(args.workflowconfig, 'r')
    config_name_split = args.workflowconfig.split('.')
    if config_name_split[1] == 'yaml':
        interface_dict = yaml.load(input_stream, Loader=yaml.Loader)
    elif config_name_split[1] == 'json':
        interface_dict = json.load(input_stream)
    for k, v in interface_dict.items():
        routine_interface = deepcopy(v)
    routine_number = len(interface_dict['nodes'])
    subroutine_number = [0 for y in range(routine_number)]
    routine_name = [0 for y in range(routine_number)]
    for i in range(routine_number):
        subroutine_number[i] = len(routine_interface[i]['tasks'])
    function = [['null' for x in range(subroutine_number[y])]
                for y in range(routine_number)]
    function_noduplicate = [['null' for x in range(subroutine_number[y])]
                            for y in range(routine_number)]
    input_files = [['null' for x in range(subroutine_number[y])]
                   for y in range(routine_number)]
    input_files_noduplicate = [['null' for x in range(subroutine_number[y])]
                               for y in range(routine_number)]
    input_files_nodependency = [['null' for x in range(subroutine_number[y])]
                                for y in range(routine_number)]
    input_files_localname = [['null' for x in range(subroutine_number[y])]
                             for y in range(routine_number)]
    input_files_links = [['null' for x in range(subroutine_number[y])]
                         for y in range(routine_number)]
    input_dependency_id = [['null' for x in range(subroutine_number[y])]
                           for y in range(routine_number)]
    output_files = [['null' for x in range(subroutine_number[y])]
                    for y in range(routine_number)]
    output_files_noduplicate = [['null' for x in range(subroutine_number[y])]
                                for y in range(routine_number)]
    output_files_nodependency = [['null' for x in range(subroutine_number[y])]
                                 for y in range(routine_number)]
    output_files_localname = [['null' for x in range(subroutine_number[y])]
                              for y in range(routine_number)]
    kwargs = [['null' for x in range(subroutine_number[y])]
              for y in range(routine_number)]
    kwargs_noduplicate = [['null' for x in range(subroutine_number[y])]
                          for y in range(routine_number)]

    # Start of yaml parser
    for i in range(routine_number):
        for j in range(len(routine_interface[i]['tasks'])):
            function[i][j] = deepcopy(
                routine_interface[i]['tasks'][j]['func'])
            function_noduplicate[i][j] = deepcopy(
                routine_interface[i]['tasks'][j]['func'][1])
        function_noduplicate[i][:] = extractlist(function_noduplicate[i][:])

    for i in range(routine_number):
        function_noduplicate[i][:] = duplicate_finder(
            function_noduplicate[i][:])

    for i in range(routine_number):
        for j in range(len(routine_interface[i]['tasks'])):
            input_files[i][j] = deepcopy(
                routine_interface[i]['tasks'][j]['inputs'])
            input_files_noduplicate[i][j] = deepcopy(
                routine_interface[i]['tasks'][j]['inputs'])
            input_files_nodependency[i][j] = deepcopy(
                routine_interface[i]['tasks'][j]['inputs'])
            input_files_links[i][j] = deepcopy(
                routine_interface[i]['tasks'][j]['inputs'])
            input_files_localname[i][j] = deepcopy(
                routine_interface[i]['tasks'][j]['inputs'])

    for i in range(routine_number):
        input_files_noduplicate[i][:] = duplicate_finder(
            input_files_noduplicate[i][:])

    for i in range(routine_number):
        for j in range(len(routine_interface[i]['tasks'])):
            output_files[i][j] = deepcopy(
                routine_interface[i]['tasks'][j]['outputs'])
            output_files_noduplicate[i][j] = deepcopy(
                routine_interface[i]['tasks'][j]['outputs'])
            output_files_localname[i][j] = deepcopy(
                routine_interface[i]['tasks'][j]['outputs'])
    for i in range(routine_number):
        output_files_noduplicate[i][:] = duplicate_finder(
            output_files_noduplicate[i][:])

    for i in range(routine_number):
        for j in range(len(routine_interface[i]['tasks'])):
            for k in range(len(input_files_localname[i][j])):
                input_files_localname[i][j][k] = input_files_localname[i][j][k].rsplit('_', 1)[
                    0]

    for i in range(routine_number):
        for j in range(len(routine_interface[i]['tasks'])):
            for k in range(len(output_files_localname[i][j])):
                output_files_localname[i][j][k] = output_files_localname[i][j][k].rsplit('_', 1)[
                    0]

    for i in range(routine_number):
        for j in range(len(routine_interface[i]['tasks'])):
            kwargs[i][j] = deepcopy(routine_interface[i]['tasks'][j]['kwargs'])
            kwargs_noduplicate[i][j] = deepcopy(
                routine_interface[i]['tasks'][j]['kwargs'])
    for i in range(routine_number):
        kwargs_noduplicate[i][:] = duplicate_finder(kwargs_noduplicate[i][:])

    for i in range(routine_number):
        routine_name[i] = routine_interface[i]['name']

    for i in range(routine_number):
        input_files_nodependency[i] = dependency_check(
            input_files_nodependency[i][:], output_files[i][:])
        input_files_links[i] = dependency_check(
            input_files_links[i][:], output_files[i][:])

    for i in range(routine_number):
        for j in range(routine_number):
            if i != j:
                input_files_links[i][:] = link_check(
                    input_files_links[i][:], input_files_nodependency[i][:], output_files[j][:], j)

    workflow_path = os.path.join(
        args.outputpath,
        interface_dict['workflow_name'])

    if not os.path.exists(workflow_path):
        os.makedirs(workflow_path)
    else:
        shutil.rmtree(workflow_path)
        os.makedirs(workflow_path)

    if args.wms == 'simstack' or args.wms == 'all':
        for i in range(routine_number):
            simstack_path = os.path.join(workflow_path, 'simstack')
            wano_path = os.path.join(simstack_path, 'wanos', routine_name[i])
            os.makedirs(wano_path)
            wrapper_path = os.path.join(
                wano_path, routine_name[i] + '_wrapper.py')
            file = open(wrapper_path, 'w')
            file.write("import yaml\n")
            file.write("import numpy as np\n")
            for j in range(subroutine_number[i]):
                if function[i][j][0] != 'BUILTIN' and function_noduplicate[i][j] != 'duplicate':
                    file.write('from ' + str(function[i][j][0]) +
                               ' import ' + str(function[i][j][1]) + '\n')
            file.write("\nif __name__ == '__main__':\n\n")

            for j in range(subroutine_number[i]):
                for k in range(len(input_files_noduplicate[i][j])):
                    if input_files_noduplicate[i][j][k] != 'duplicate' and input_files_nodependency[i][
                            j][k] != 'inner_dependent' and input_files_links[i][j][k][0].isnumeric() == False:
                        file.write('\n \t#Read Input #' +
                                   str(k +
                                       1) +
                                   ' from subroutine #' +
                                   str(j +
                                       1) +
                                   ' in routine #' +
                                   str(i) +
                                   '\n')
                        file.write(
                            "\tyaml_stream = open('" +
                            input_files[i][j][k] +
                            ".yaml', 'r')\n")
                        file.write(
                            "\t" +
                            input_files[i][j][k] +
                            " = yaml.load(yaml_stream, Loader=yaml.Loader)\n\n")
                    if input_files_noduplicate[i][j][k] != 'duplicate' and input_files_nodependency[
                            i][j][k] != 'inner_dependent' and input_files_links[i][j][k][0].isnumeric():
                        file.write('\n \t#Read Input #' +
                                   str(k +
                                       1) +
                                   ' from subroutine #' +
                                   str(j +
                                       1) +
                                   ' in routine #' +
                                   str(i) +
                                   '\n')
                        file.write(
                            "\tyaml_stream = open('" +
                            input_files_localname[i][j][k] +
                            ".yaml', 'r')\n")
                        file.write(
                            "\t" +
                            input_files_localname[i][j][k] +
                            " = yaml.load(yaml_stream, Loader=yaml.Loader)\n\n")

                if function[i][j][0] != 'BUILTIN':
                    file.write('\n\t# Call subroutine #' + str(j + 1) + '\n')
                    file.write('\t')
                    for k in range(len(output_files[i][j])):
                        if k != len(output_files[i][j]) - 1:
                            file.write(output_files_localname[i][j][k] + ", ")
                        else:
                            file.write(output_files_localname[i][j][k] + " = ")
                    file.write(function[i][j][1] + "(")
                    for k in range(len(input_files[i][j])):
                        if k != len(
                                input_files[i][j]) - 1 and input_files_nodependency[i][j][k] != 'inner_dependent' and input_files_links[i][j][k][0].isnumeric() == False:
                            file.write(str(input_files[i][j][k]) + ', ')
                        elif k != len(input_files[i][j]) - 1 and (input_files_nodependency[i][j][k] == 'inner_dependent' or input_files_links[i][j][k][0].isnumeric()):
                            file.write(
                                str(input_files_localname[i][j][k]) + ', ')
                        elif k == len(input_files[i][j]) - 1 and input_files_nodependency[i][j][k] != 'inner_dependent' and input_files_links[i][j][k][0].isnumeric() == False:
                            file.write(str(input_files[i][j][k]) + ")\n\n")
                        elif k == len(input_files[i][j]) - 1 and (input_files_nodependency[i][j][k] == 'inner_dependent' or input_files_links[i][j][k][0].isnumeric()):
                            file.write(
                                str(input_files_localname[i][j][k]) + ")\n\n")

                    file.write(
                        "\n\t# Dump outputs into yaml files from subroutine #" + str(j + 1) + '\n')

                    for k in range(len(output_files[i][j])):
                        file.write('\n\t#Dump Output #' +
                                   str(k +
                                       1) +
                                   ' from subroutine #' +
                                   str(j +
                                       1) +
                                   ' in routine # ' +
                                   str(i) +
                                   '\n')
                        file.write(
                            "\tfile=open('" +
                            output_files_localname[i][j][k] +
                            ".yaml', 'w')\n")
                        file.write(
                            "\tyaml.dump(" +
                            output_files_localname[i][j][k] +
                            ", file, default_flow_style=False)\n")
                        file.write("\tfile.close\n")
                    file.write("\t#End of subroutine #" +
                               str(j + 1) + ' in routine # ' + str(i) + '\n')

                elif function[i][j][0] == 'BUILTIN' and function[i][j][1] == 'MERGE':
                    file.write("\t# Merge Multiple Dictionaries \n")
                    for k in range(len(input_files[i][j])):
                        if input_files_noduplicate[i][j][k] != 'duplicate' and input_files_nodependency[i][
                                j][k] != 'inner_dependent' and input_files_links[i][j][k][0].isnumeric() == False:
                            file.write(
                                "if isinstance(" + input_files[i][j][k] + ", np.number):\n")
                            file.write(
                                '\t' +
                                input_files_localname[i][j][k] +
                                ' = dict(' +
                                input_files_localname[i][j][k] +
                                '= float(' +
                                input_files[i][j][k] +
                                ')) \n')
                        elif input_files_noduplicate[i][j][k] != 'duplicate' and (input_files_nodependency[i][j][k] == 'inner_dependent' or input_files_links[i][j][k][0].isnumeric()):
                            file.write(
                                "if isinstance(" + input_files_localname[i][j][k] + ", np.number):\n")
                            file.write(
                                '\t' +
                                input_files_localname[i][j][k] +
                                ' = dict(' +
                                input_files_localname[i][j][k] +
                                '= float(' +
                                input_files_localname[i][j][k] +
                                ')) \n')
                        if input_files_noduplicate[i][j][k] != 'duplicate' and input_files_nodependency[i][
                                j][k] != 'inner_dependent' and input_files_links[i][j][k][0].isnumeric() == False:
                            file.write("else:\n")
                            file.write(
                                '\t' +
                                input_files_localname[i][j][k] +
                                ' = dict(' +
                                input_files_localname[i][j][k] +
                                '= ' +
                                input_files[i][j][k] +
                                ') \n\n')
                        elif input_files_noduplicate[i][j][k] != 'duplicate' and (input_files_nodependency[i][j][k] == 'inner_dependent' or input_files_links[i][j][k][0].isnumeric()):
                            file.write("else:\n")
                            file.write(
                                '\t' +
                                input_files_localname[i][j][k] +
                                ' = dict(' +
                                input_files_localname[i][j][k] +
                                '= ' +
                                input_files_localname[i][j][k] +
                                ') \n\n')

                    file.write('\t' + output_files_localname[i][j][0] + '={')
                    for k in range(len(input_files[i][j])):
                        if input_files_noduplicate[i][j][k] != 'duplicate':
                            if k != len(input_files[i][j]) - 1:
                                file.write(
                                    '**' + input_files_localname[i][j][k] + ',')
                            elif k == len(input_files[i][j]) - 1:
                                file.write(
                                    '**' + input_files_localname[i][j][k] + '}')

                    file.write(
                        '\n\n\t#Dump Output from Merged Dictionaries #\n')
                    file.write(
                        "\tfile=open('" +
                        output_files_localname[i][j][0] +
                        ".yaml', 'w')\n")
                    file.write(
                        "\tyaml.dump(" +
                        output_files_localname[i][j][0] +
                        ", file, default_flow_style=False)\n")
                    file.write("\tfile.close()\n")
                    file.write("\t#End of subroutine #" +
                               str(j + 1) + ' in routine # ' + str(i) + '\n')
        file.close()

        for i in range(routine_number):
            xml_path = os.path.join(
                simstack_path,
                'wanos',
                routine_name[i],
                routine_name[i] +
                '.xml')
            # Strat xml file generation
            file = open(xml_path, 'w')
            file.write('<WaNoTemplate>\n')
            file.write(" <WaNoRoot name='" + routine_name[i] + "'>\n")

            for j in range(subroutine_number[i]):
                for k in range(len(input_files_noduplicate[i][j])):
                    if input_files_noduplicate[i][j][k] != \
                            'duplicate' and input_files_nodependency[i][j][k] != 'inner_dependent'\
                            and input_files_links[i][j][k][0].isnumeric() == False:
                        file.write(
                            "\t<WaNoFile name='" +
                            input_files_localname[i][j][k] +
                            " Data' logical_filename='" +
                            input_files_noduplicate[i][j][k] +
                            ".yaml' local='True'>" +
                            args.inputpath +
                            input_files_noduplicate[i][j][k] +
                            ".yaml</WaNoFile> \n")
                    elif input_files_noduplicate[i][j][k] != \
                        'duplicate' and input_files_nodependency[i][j][k] != 'inner_dependent'\
                            and input_files_links[i][j][k][0].isnumeric():
                        path_dependent_output = os.path.join(routine_name[int(input_files_links[i][j][k][0])], output_files_localname[int(
                            input_files_links[i][j][k][0])][input_files_links[i][j][k][1]][input_files_links[i][j][k][2]] + '.yaml')
                        file.write(
                            "\t<WaNoFile name='" +
                            input_files_localname[i][j][k] +
                            " Data' logical_filename='" +
                            input_files_localname[i][j][k] +
                            ".yaml' local='False'>" +
                            path_dependent_output +
                            "</WaNoFile> \n")

            file.write(' </WaNoRoot>\n')
            file.write(
                " <WaNoExecCommand> bash ./run_python.sh</WaNoExecCommand>\n")
            file.write(" <WaNoInputFiles>\n")
            file.write("\t<WaNoInputFile logical_filename='run_python.sh'>"
                       "run_python.sh</WaNoInputFile>\n")
            file.write(
                "\t<WaNoInputFile logical_filename='" +
                routine_name[i] +
                "_wrapper.py'>" +
                routine_name[i] +
                "_wrapper.py</WaNoInputFile>\n")
            file.write(
                "\t<WaNoInputFile logical_filename='setenv.sh'>setenv.sh</WaNoInputFile>\n")
            file.write(" </WaNoInputFiles>\n")
            file.write(" <WaNoOutputFiles>\n")
            for j in range(subroutine_number[i]):
                for k in range(len(output_files_noduplicate[i][j])):
                    if output_files_noduplicate[i][j][k] != 'duplicate':
                        file.write(
                            "\t<WaNoOutputFile logical_filename='" +
                            output_files_localname[i][j][k] +
                            ".yaml'>" +
                            output_files_localname[i][j][k] +
                            ".yaml</WaNoOutputFile>\n")
            file.write(" </WaNoOutputFiles>\n")
            file.write("</WaNoTemplate>")

            file.close()
            # Validate xml against schema
            schema_path = os.path.join(
                os.path.dirname(
                    os.path.abspath(__file__)),
                'xmlschema.xsd')
            my_schema = xmlschema.XMLSchema(schema_path)
            my_schema.validate(xml_path)

        simstack_wf_path = os.path.join(simstack_path, 'simstack.xml')
        file = open(simstack_wf_path, 'w')
        file.write('<root> \n')
        for i in range(routine_number):
            file.write(
                "\t <WaNo type='" +
                routine_name[i] +
                "' uuid='" +
                routine_name[i] +
                "' id='" +
                str(i) +
                "' name='" +
                routine_name[i] +
                "'/> \n")
        file.write('</root> \n')

        for i in range(routine_number):
            run_python_path = os.path.join(
                simstack_path, 'wanos', routine_name[i], 'run_python.sh')
            file = open(run_python_path, 'w')
            file.write("#!/bin/bash/ -ex \n")
            file.write("source setenv.sh \n")
            file.write("python " + routine_name[i] + "_wrapper.py")

            file.close()

            for i in range(routine_number):
                dest_path = os.path.join(
                    simstack_path, 'wanos', routine_name[i])
                source_path = os.path.join(
                    os.path.dirname(
                        os.path.abspath(__file__)),
                    'setenv.sh ')
                cmd = 'cp ' + source_path + dest_path
                os.popen(cmd)
                source_path = os.path.join(
                    os.path.dirname(
                        os.path.abspath(__file__)),
                    '..',
                    'fig',
                    'wano_fig.png ')
                cmd = 'cp ' + source_path + dest_path
                os.popen(cmd)
                source_path = os.path.join(
                    os.path.dirname(
                        os.path.abspath(__file__)),
                    'resources.yml ')
                cmd = 'cp ' + source_path + dest_path
                os.popen(cmd)

        # End of WaNo Folder generation

    if args.wms == 'firework' or args.wms == 'all':
        firework_path = os.path.join(workflow_path, 'firework')
        os.makedirs(firework_path)
        fws = []
        for i in range(routine_number):
            fw = {}
            fw['fw_id'] = i
            fw['name'] = routine_name[i]
            fw['spec'] = {}
            fw['spec']['_tasks'] = []
            output_subroutine_iter = iter(output_files_localname[i])
            input_subroutine_iter = iter(input_files_localname[i])
            subroutine_iter = iter(function[i])
            # iterate over the tasklist
            for j in range(subroutine_number[i]):
                task = {}
                subroutine = next(subroutine_iter)
                if subroutine[0] != 'BUILTIN':
                    task['_fw_name'] = 'PyTask'
                    task['func'] = str(subroutine[0]) + \
                        '.' + str(subroutine[1])
                elif subroutine[1] == 'MERGE':
                    task['_fw_name'] = 'JoinDictTask'
                task['inputs'] = []
                input = next(input_subroutine_iter)
                task['inputs'] = input
                output = next(output_subroutine_iter)
                if subroutine[0] != 'BUILTIN':
                    task['outputs'] = []
                    task['outputs'] = output
                elif subroutine[1] == 'MERGE':
                    task['output'] = []
                    task['output'] = output[0]
                fw['spec']['_tasks'].append(task)
                # end of tasklist iterator

            for j in range(subroutine_number[i]):
                for k in range(len(input_files[i][j])):
                    if (input_files_nodependency[i][j][k] != 'inner_dependent' and
                        input_files_noduplicate[i][j][k] != 'duplicate' and
                            input_files_links[i][j][k][0].isnumeric() == False):
                        input_string = os.path.join(
                            args.inputpath, str(
                                input_files[i][j][k]) + '.yaml')
                        with open(input_string, 'r') as input_stream:
                            fw['spec'][input_files_localname[i][j]
                                       [k]] = yaml.safe_load(input_stream)
            fws.append(fw)
        links = {}
        links_dot = {}

        for M in range(routine_number):
            links[str(M)] = []
            links_dot[str(M)] = []
            for i in range(routine_number):
                for j in range(subroutine_number[i]):
                    for k in range(len(input_files_links[i][j])):
                        if isinstance(
                                input_files_links[i][j][k],
                                list) and input_files_links[i][j][k][0] == str(M):
                            links[str(M)].append(i)
            links_dot[str(M)] = links[str(M)]
            links[str(M)] = list(OrderedDict.fromkeys(links[str(M)]))
        metadata = {}
        workflow = {'fws': fws, 'links': links, 'metadata': metadata,
                    'name': interface_dict['workflow_name'] + '_wfGenes'}

        fireworks_schema.validate(workflow, 'Workflow')

        pad_path = os.path.join(firework_path,
                                interface_dict['workflow_name'] + '.yaml')
        with open(pad_path, 'w') as output_stream:
            yaml.dump(workflow, output_stream)


        
        dot_folder = os.path.join(workflow_path, 'DOT')
        if not os.path.exists(dot_folder):
            os.makedirs(dot_folder)
        

        
        dot_file = os.path.join(dot_folder,
            interface_dict['workflow_name'] + '.dot')
        
        
        dot_string = 'digraph {\n'
        dot_string += '  graph[\n'
        dot_string += '\tname=' + interface_dict['workflow_name'] + '\n'
        dot_string += '  ]; \n'
        for i in range(routine_number):
            dot_string += str(i) + ' [' + '\n'
            dot_string += 'state=NONE \n'
            dot_string += 'name=' + routine_name[i] + '\n'
            dot_string += 'label=' + routine_name[i] + '\n'
            dot_string += '  ]; \n'

        for i in range(routine_number):
            for j in range(subroutine_number[i]):
                for k in range(len(input_files[i][j])):
                    if input_files_noduplicate[i][j][k] != \
                        'duplicate' and input_files_nodependency[i][j][k] != 'dependent'\
                            and input_files_links[i][j][k][0].isnumeric():
                        dot_string += input_files_links[i][j][k][0] + \
                            '->' + str(i) + '[ \n'
                        dot_string += 'label=' + \
                            input_files_localname[i][j][k] + '\n'
                        dot_string += '  ]; \n'

        dot_string += '  } \n'

        with open(dot_file, 'w') as file:
            file.write(dot_string)
        pdf_file = os.path.join(
            dot_folder , interface_dict['workflow_name'] + '.pdf')
        cmd = 'dot -Tps ' + dot_file + ' -o ' + pdf_file
        os.popen(cmd)

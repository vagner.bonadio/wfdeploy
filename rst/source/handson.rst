.. _handson: 


Handson on wfGenes
=======================
This documents goes through five examples to examine wfGenes and supported WMSs using JupyterLab interface.

:ref:`Simple Workflow <simple_workflow>` 
=====================================

In order to get started with wfGenes, this example goes through generation to execution phases by constructing simple workflow running python and script tasks.  


:ref:`Foreach example <foreach_example>`
=====================================

This example introduces convenient method to build workflows with internal data palatalization using different WMSs scaling from personal laptops to supercomputers with thousands of cores.     


:ref:`Random task graph <rgg>`
=====================================

Random task graph example demonstrate the capabilities of wfGenes in task coupling and palatalization by translating random graph (DAG) with thousands of node to different workflow management systems. 


.. _bill:


Export and import files in SimStack
====================================

In this example we practice how to pass input to source code using  input files in any format depending on your application and simulation domain. We will set our workflow using SimStack GUI and rendered WaNo which needs input module to complete the simulation. For the sake of time, we compute a restaurant bill for three different costumers. All data related to this computation including  customer orders and restaurant menu is included in a **input_file.py** file.  Obviously, this example can be extended and adapted based on your application while same method is applied for parsing large input files, I.e. Gigabyte (GB) and Terabyte (TB) files. In brief you can gain following skills by completing this exercise:

1. Importing file to your WaNo.
2. Exporting output and examine standard error output.
3. Modifying WaNo template to adapt to simulation files.


Firstly, drag and drop Bill WaNo to nanomatch pane of simstack, open the WaNo setting by double clicking on the WaNo. Afterwards, import **input_file.py** by navigating to Bill folder in exercise document. Save your workflow and run, please check generated output files and monitor your job status. As a designed example, your simulation fails due to wrong configuration. Please try to resolve the issue by checking your xml file and standard error output log. 

.. image:: /fig/Bill.png 
   :width: 800
   :height: 400 


After completing this example, you are ready to move to :ref:`next example <workflow>` to integrate two different WaNo and run multiple job within one session.

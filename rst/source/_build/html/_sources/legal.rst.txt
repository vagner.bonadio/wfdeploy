.. _legal: 


Legal Notice:
=============

The tutorial documentation is licensed under an
Attribution-NonCommercial-NoDerivatives 4.0 International
Creative Commons License
http://creativecommons.org/licenses/by-nc-nd/4.0/

Copyright © 2019 Karlsruhe Institute of Technology (KIT)
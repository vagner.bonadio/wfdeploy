.. _install_client: 

Client Installation
===================

In order to install SimStack client, you need to download the client component by running: 

.. code-block:: bash

    scp USERNAME@Client:/shared/simstack/2019-10-14-simstack_linux.tar.gz ./

Next, you should unzip the folder: 

.. code-block:: bash

    tar -xzf 2019-10-14-simstack_linux.tar.gz

Afterwards, navigate to simstack_linux folder and run **./run-simstack.sh** to perform the installation. You will see license agreement that you can skip with pressing **q** and accept it with **y**. Please wait while installation may take few minutes to complete. After successful installation SimStack will appear for the first time. 

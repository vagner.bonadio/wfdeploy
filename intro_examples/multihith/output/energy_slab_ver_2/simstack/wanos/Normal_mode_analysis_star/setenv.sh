#!/bin/bash/ -ex
#module purge
conda activate myenv
#module load chem/turbomole
#module load chem/vasp/
#export VASP_COMMAND="$DO_PARALLEL $VASPMPI"
#export ASE_VASP_VDW=$VASP_HOME/bin
export PYTHONPATH=/home/mehdi/work/gitlab/workflow_generator/intro_examples/multihith/lib:$PYTHONPATH
export PYTHONPATH=/home/mehdi/work/gitlab/workflow_generator/wfGenes_exe/:$PYTHONPATH
export PYTHONPATH=/home/mehdi/work/gitlab/workflow_generator/intro_examples/foreach_sample/lib:$PYTHONPATH

#unset I_MPI_HYDRA_BOOTSTRAP I_MPI_HYDRA_RMK I_MPI_HYDRA_BRANCH_COUNT
#export I_MPI_HYDRA_BOOTSTRAP=ssh


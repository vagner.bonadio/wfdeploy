import yaml
import numpy as np
from ueffumax import build_structure_generic
from ueffumax import structure_relaxation

if __name__ == '__main__':


 	#Read Input #1 from subroutine #1 in routine #0
	yaml_stream = open('parameters_id0.yaml', 'r')
	parameters_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


	# Call subroutine #1
	structure = build_structure_generic(parameters_id0)


	# Dump outputs into yaml files from subroutine #1

	#Dump Output #1 from subroutine #1 in routine # 0
	file=open('structure.yaml', 'w')
	yaml.dump(structure, file, default_flow_style=False)
	file.close
	#End of subroutine #1 in routine # 0

 	#Read Input #2 from subroutine #2 in routine #0
	yaml_stream = open('structure_id0.yaml', 'r')
	structure_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


	# Call subroutine #2
	relaxed_structure, energy, forces, dipole = structure_relaxation(parameters_id0, structure_id0)


	# Dump outputs into yaml files from subroutine #2

	#Dump Output #1 from subroutine #2 in routine # 0
	file=open('relaxed_structure.yaml', 'w')
	yaml.dump(relaxed_structure, file, default_flow_style=False)
	file.close

	#Dump Output #2 from subroutine #2 in routine # 0
	file=open('energy.yaml', 'w')
	yaml.dump(energy, file, default_flow_style=False)
	file.close

	#Dump Output #3 from subroutine #2 in routine # 0
	file=open('forces.yaml', 'w')
	yaml.dump(forces, file, default_flow_style=False)
	file.close

	#Dump Output #4 from subroutine #2 in routine # 0
	file=open('dipole.yaml', 'w')
	yaml.dump(dipole, file, default_flow_style=False)
	file.close
	#End of subroutine #2 in routine # 0

 	#Read Input #1 from subroutine #3 in routine #0
	yaml_stream = open('energy_id0.yaml', 'r')
	energy_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #2 from subroutine #3 in routine #0
	yaml_stream = open('zero_point_energy_id0.yaml', 'r')
	zero_point_energy_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #3 from subroutine #3 in routine #0
	yaml_stream = open('total_entropy_term_id0.yaml', 'r')
	total_entropy_term_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #4 from subroutine #3 in routine #0
	yaml_stream = open('energy_minimum_id0.yaml', 'r')
	energy_minimum_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)

	# Merge Multiple Dictionaries 
if isinstance(energy_id0, np.number):
	energy = dict(energy= float(energy_id0)) 
else:
	energy = dict(energy= energy_id0) 

if isinstance(zero_point_energy_id0, np.number):
	zero_point_energy = dict(zero_point_energy= float(zero_point_energy_id0)) 
else:
	zero_point_energy = dict(zero_point_energy= zero_point_energy_id0) 

if isinstance(total_entropy_term_id0, np.number):
	total_entropy_term = dict(total_entropy_term= float(total_entropy_term_id0)) 
else:
	total_entropy_term = dict(total_entropy_term= total_entropy_term_id0) 

if isinstance(energy_minimum_id0, np.number):
	energy_minimum = dict(energy_minimum= float(energy_minimum_id0)) 
else:
	energy_minimum = dict(energy_minimum= energy_minimum_id0) 

	o_two={**energy,**zero_point_energy,**total_entropy_term,**energy_minimum}

	#Dump Output from Merged Dictionaries #
	file=open('o_two.yaml', 'w')
	yaml.dump(o_two, file, default_flow_style=False)
	file.close()
	#End of subroutine #3 in routine # 0

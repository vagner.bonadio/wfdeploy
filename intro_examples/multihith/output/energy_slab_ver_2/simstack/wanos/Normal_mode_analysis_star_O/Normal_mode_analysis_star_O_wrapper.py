import yaml
import numpy as np
from ueffumax import build_structure_generic
from ueffumax import structure_relaxation

if __name__ == '__main__':


 	#Read Input #1 from subroutine #1 in routine #0
	yaml_stream = open('parameters_id0.yaml', 'r')
	parameters_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


	# Call subroutine #1
	structure = build_structure_generic(parameters_id0)


	# Dump outputs into yaml files from subroutine #1

	#Dump Output #1 from subroutine #1 in routine # 0
	file=open('structure.yaml', 'w')
	yaml.dump(structure, file, default_flow_style=False)
	file.close
	#End of subroutine #1 in routine # 0

 	#Read Input #2 from subroutine #2 in routine #0
	yaml_stream = open('structure_id0.yaml', 'r')
	structure_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


	# Call subroutine #2
	relaxed_structure, energy, forces, dipole = structure_relaxation(parameters_id0, structure_id0)


	# Dump outputs into yaml files from subroutine #2

	#Dump Output #1 from subroutine #2 in routine # 0
	file=open('relaxed_structure.yaml', 'w')
	yaml.dump(relaxed_structure, file, default_flow_style=False)
	file.close

	#Dump Output #2 from subroutine #2 in routine # 0
	file=open('energy.yaml', 'w')
	yaml.dump(energy, file, default_flow_style=False)
	file.close

	#Dump Output #3 from subroutine #2 in routine # 0
	file=open('forces.yaml', 'w')
	yaml.dump(forces, file, default_flow_style=False)
	file.close

	#Dump Output #4 from subroutine #2 in routine # 0
	file=open('dipole.yaml', 'w')
	yaml.dump(dipole, file, default_flow_style=False)
	file.close
	#End of subroutine #2 in routine # 0

 	#Read Input #1 from subroutine #3 in routine #0
	yaml_stream = open('energy_id0.yaml', 'r')
	energy_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #2 from subroutine #3 in routine #0
	yaml_stream = open('zero_point_energy_id0.yaml', 'r')
	zero_point_energy_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #3 from subroutine #3 in routine #0
	yaml_stream = open('total_entropy_term_id0.yaml', 'r')
	total_entropy_term_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #4 from subroutine #3 in routine #0
	yaml_stream = open('energy_minimum_id0.yaml', 'r')
	energy_minimum_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)

	# Merge Multiple Dictionaries 
if isinstance(energy_id0, np.number):
	energy = dict(energy= float(energy_id0)) 
else:
	energy = dict(energy= energy_id0) 

if isinstance(zero_point_energy_id0, np.number):
	zero_point_energy = dict(zero_point_energy= float(zero_point_energy_id0)) 
else:
	zero_point_energy = dict(zero_point_energy= zero_point_energy_id0) 

if isinstance(total_entropy_term_id0, np.number):
	total_entropy_term = dict(total_entropy_term= float(total_entropy_term_id0)) 
else:
	total_entropy_term = dict(total_entropy_term= total_entropy_term_id0) 

if isinstance(energy_minimum_id0, np.number):
	energy_minimum = dict(energy_minimum= float(energy_minimum_id0)) 
else:
	energy_minimum = dict(energy_minimum= energy_minimum_id0) 

	o_two={**energy,**zero_point_energy,**total_entropy_term,**energy_minimum}

	#Dump Output from Merged Dictionaries #
	file=open('o_two.yaml', 'w')
	yaml.dump(o_two, file, default_flow_style=False)
	file.close()
	#End of subroutine #3 in routine # 0
import yaml
import numpy as np
from ueffumax import build_structure

if __name__ == '__main__':


 	#Read Input #1 from subroutine #1 in routine #1
	yaml_stream = open('parameters_id1.yaml', 'r')
	parameters_id1 = yaml.load(yaml_stream, Loader=yaml.Loader)


	# Call subroutine #1
	structure = build_structure(parameters_id1)


	# Dump outputs into yaml files from subroutine #1

	#Dump Output #1 from subroutine #1 in routine # 1
	file=open('structure.yaml', 'w')
	yaml.dump(structure, file, default_flow_style=False)
	file.close
	#End of subroutine #1 in routine # 1
import yaml
import numpy as np
from ueffumax import structure_relaxation

if __name__ == '__main__':


 	#Read Input #1 from subroutine #1 in routine #2
	yaml_stream = open('parameters_id2.yaml', 'r')
	parameters_id2 = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #2 from subroutine #1 in routine #2
	yaml_stream = open('structure.yaml', 'r')
	structure = yaml.load(yaml_stream, Loader=yaml.Loader)


	# Call subroutine #1
	relaxed_structure, energy, forces, dipole = structure_relaxation(parameters_id2, structure)


	# Dump outputs into yaml files from subroutine #1

	#Dump Output #1 from subroutine #1 in routine # 2
	file=open('relaxed_structure.yaml', 'w')
	yaml.dump(relaxed_structure, file, default_flow_style=False)
	file.close

	#Dump Output #2 from subroutine #1 in routine # 2
	file=open('energy.yaml', 'w')
	yaml.dump(energy, file, default_flow_style=False)
	file.close

	#Dump Output #3 from subroutine #1 in routine # 2
	file=open('forces.yaml', 'w')
	yaml.dump(forces, file, default_flow_style=False)
	file.close

	#Dump Output #4 from subroutine #1 in routine # 2
	file=open('dipole.yaml', 'w')
	yaml.dump(dipole, file, default_flow_style=False)
	file.close
	#End of subroutine #1 in routine # 2
import yaml
import numpy as np
from ueffumax import vibrations

if __name__ == '__main__':


 	#Read Input #1 from subroutine #1 in routine #3
	yaml_stream = open('parameters_id3.yaml', 'r')
	parameters_id3 = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #2 from subroutine #1 in routine #3
	yaml_stream = open('relaxed_structure.yaml', 'r')
	relaxed_structure = yaml.load(yaml_stream, Loader=yaml.Loader)


	# Call subroutine #1
	vibrational_energies, vibrational_entropy, vibrational_entropy_term, vibrational_partition_func, zero_point_energy, transition_state, energy_minimum = vibrations(parameters_id3, relaxed_structure)


	# Dump outputs into yaml files from subroutine #1

	#Dump Output #1 from subroutine #1 in routine # 3
	file=open('vibrational_energies.yaml', 'w')
	yaml.dump(vibrational_energies, file, default_flow_style=False)
	file.close

	#Dump Output #2 from subroutine #1 in routine # 3
	file=open('vibrational_entropy.yaml', 'w')
	yaml.dump(vibrational_entropy, file, default_flow_style=False)
	file.close

	#Dump Output #3 from subroutine #1 in routine # 3
	file=open('vibrational_entropy_term.yaml', 'w')
	yaml.dump(vibrational_entropy_term, file, default_flow_style=False)
	file.close

	#Dump Output #4 from subroutine #1 in routine # 3
	file=open('vibrational_partition_func.yaml', 'w')
	yaml.dump(vibrational_partition_func, file, default_flow_style=False)
	file.close

	#Dump Output #5 from subroutine #1 in routine # 3
	file=open('zero_point_energy.yaml', 'w')
	yaml.dump(zero_point_energy, file, default_flow_style=False)
	file.close

	#Dump Output #6 from subroutine #1 in routine # 3
	file=open('transition_state.yaml', 'w')
	yaml.dump(transition_state, file, default_flow_style=False)
	file.close

	#Dump Output #7 from subroutine #1 in routine # 3
	file=open('energy_minimum.yaml', 'w')
	yaml.dump(energy_minimum, file, default_flow_style=False)
	file.close
	#End of subroutine #1 in routine # 3

 	#Read Input #1 from subroutine #2 in routine #3
	yaml_stream = open('energy_minimum_id3.yaml', 'r')
	energy_minimum_id3 = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #2 from subroutine #2 in routine #3
	yaml_stream = open('energy.yaml', 'r')
	energy = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #3 from subroutine #2 in routine #3
	yaml_stream = open('zero_point_energy_id3.yaml', 'r')
	zero_point_energy_id3 = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #4 from subroutine #2 in routine #3
	yaml_stream = open('vibrational_entropy_term_id3.yaml', 'r')
	vibrational_entropy_term_id3 = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #5 from subroutine #2 in routine #3
	yaml_stream = open('rotational_entropy_term_id3.yaml', 'r')
	rotational_entropy_term_id3 = yaml.load(yaml_stream, Loader=yaml.Loader)


 	#Read Input #6 from subroutine #2 in routine #3
	yaml_stream = open('translational_entropy_term_id3.yaml', 'r')
	translational_entropy_term_id3 = yaml.load(yaml_stream, Loader=yaml.Loader)

	# Merge Multiple Dictionaries 
if isinstance(energy_minimum_id3, np.number):
	energy_minimum = dict(energy_minimum= float(energy_minimum_id3)) 
else:
	energy_minimum = dict(energy_minimum= energy_minimum_id3) 

if isinstance(energy, np.number):
	energy = dict(energy= float(energy)) 
else:
	energy = dict(energy= energy) 

if isinstance(zero_point_energy_id3, np.number):
	zero_point_energy = dict(zero_point_energy= float(zero_point_energy_id3)) 
else:
	zero_point_energy = dict(zero_point_energy= zero_point_energy_id3) 

if isinstance(vibrational_entropy_term_id3, np.number):
	vibrational_entropy_term = dict(vibrational_entropy_term= float(vibrational_entropy_term_id3)) 
else:
	vibrational_entropy_term = dict(vibrational_entropy_term= vibrational_entropy_term_id3) 

if isinstance(rotational_entropy_term_id3, np.number):
	rotational_entropy_term = dict(rotational_entropy_term= float(rotational_entropy_term_id3)) 
else:
	rotational_entropy_term = dict(rotational_entropy_term= rotational_entropy_term_id3) 

if isinstance(translational_entropy_term_id3, np.number):
	translational_entropy_term = dict(translational_entropy_term= float(translational_entropy_term_id3)) 
else:
	translational_entropy_term = dict(translational_entropy_term= translational_entropy_term_id3) 

	star_o={**energy_minimum,**energy,**zero_point_energy,**vibrational_entropy_term,**rotational_entropy_term,**translational_entropy_term}

	#Dump Output from Merged Dictionaries #
	file=open('star_o.yaml', 'w')
	yaml.dump(star_o, file, default_flow_style=False)
	file.close()
	#End of subroutine #2 in routine # 3

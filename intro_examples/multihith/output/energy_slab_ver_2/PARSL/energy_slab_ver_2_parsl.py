import yaml
import numpy as np
from auxiliary import MERGE
from auxiliary import flat_tuple
from auxiliary import flat_list
import time
from parsl.addresses import address_by_hostname
from parsl.providers import LocalProvider
from parsl.channels import LocalChannel
from parsl.config import Config
from parsl.executors import HighThroughputExecutor
from parsl.monitoring.monitoring import MonitoringHub
import parsl

from parsl.providers import SlurmProvider
from parsl.launchers import SrunLauncher

from parsl.data_provider.files import File
from parsl.data_provider.file_noop import NoOpFileStaging
from ueffumax import build_structure_generic
from ueffumax import structure_relaxation
from ueffumax import build_structure
from ueffumax import vibrations
from ueffumax import reaction_energies

if __name__ == '__main__':


    start_time = time.time()    

    config_slurm = Config(
    executors=[
    HighThroughputExecutor(
            label="frontera_htex",
            address=address_by_hostname(),
            max_workers=56,
            provider=SlurmProvider(
                channel=LocalChannel(),
                nodes_per_block=1,
                init_blocks=1,
                partition='single',
                walltime = '00:30:00',
                launcher=SrunLauncher(),
            ),
        )
    ],

    monitoring=MonitoringHub(
    	hub_address=address_by_hostname(),
    	monitoring_debug=False,
		workflow_name = 'slab_1_slurm',
    	resource_monitoring_interval=1,
	), 
		strategy = None
	)
	
    config_local = Config(
    executors=[
        HighThroughputExecutor(
            label = "frontera_htex",
            cores_per_worker = 2,
            max_workers = 40,
            storage_access=[NoOpFileStaging()],
            provider = LocalProvider(
            parallelism = 1,
			),
           
        )
    ],

    monitoring=MonitoringHub(
    	hub_address=address_by_hostname(),
		workflow_name = 'slab_1_local',
    	monitoring_debug=False,
    	resource_monitoring_interval=1,
	), 
		strategy = None
	)
	
    parsl.load(config_local)
    #Read Input #1from subroutine #1 in routine #0
    yaml_stream = open('parameters_id0.yaml', 'r')
    parameters_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #2from subroutine #3 in routine #0
    yaml_stream = open('zero_point_energy_id0.yaml', 'r')
    zero_point_energy_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #3from subroutine #3 in routine #0
    yaml_stream = open('total_entropy_term_id0.yaml', 'r')
    total_entropy_term_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #4from subroutine #3 in routine #0
    yaml_stream = open('energy_minimum_id0.yaml', 'r')
    energy_minimum_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #1from subroutine #1 in routine #1
    yaml_stream = open('parameters_id1.yaml', 'r')
    parameters_id1 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #1from subroutine #1 in routine #2
    yaml_stream = open('parameters_id2.yaml', 'r')
    parameters_id2 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #1from subroutine #1 in routine #3
    yaml_stream = open('parameters_id3.yaml', 'r')
    parameters_id3 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #5from subroutine #2 in routine #3
    yaml_stream = open('rotational_entropy_term_id3.yaml', 'r')
    rotational_entropy_term_id3 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #6from subroutine #2 in routine #3
    yaml_stream = open('translational_entropy_term_id3.yaml', 'r')
    translational_entropy_term_id3 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #1from subroutine #1 in routine #4
    yaml_stream = open('parameters_id4.yaml', 'r')
    parameters_id4 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #1from subroutine #1 in routine #5
    yaml_stream = open('parameters_id5.yaml', 'r')
    parameters_id5 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #1from subroutine #1 in routine #6
    yaml_stream = open('parameters_id6.yaml', 'r')
    parameters_id6 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #5from subroutine #2 in routine #6
    yaml_stream = open('rotational_entropy_term_id6.yaml', 'r')
    rotational_entropy_term_id6 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #6from subroutine #2 in routine #6
    yaml_stream = open('translational_entropy_term_id6.yaml', 'r')
    translational_entropy_term_id6 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #2from subroutine #2 in routine #7
    yaml_stream = open('reactions.yaml', 'r')
    reactions = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()

    #### Start Step #1
    # Call subroutine #1 from routine #0
    
    parsl_build_structure_generic = build_structure_generic(parameters_id0) 
    # Call subroutine #1 from routine #1
    
    parsl_build_structure = build_structure(parameters_id1) 
    # Call subroutine #1 from routine #4
    
    parsl_build_structure_id4 = build_structure(parameters_id4) 

    #### Start Step #2
    # Call subroutine #2 from routine #0
    
    parsl_structure_relaxation = structure_relaxation(parameters_id0, parsl_build_structure_generic) 
    # Call subroutine #1 from routine #2
    
    parsl_structure_relaxation_id2 = structure_relaxation(parameters_id2, parsl_build_structure) 
    # Call subroutine #1 from routine #5
    
    parsl_structure_relaxation_id5 = structure_relaxation(parameters_id5, parsl_build_structure_id4) 

    #### Start Step #3
    # Call subroutine #3 from routine #0
    
    parsl_o_two_id0 = MERGE(energy_id0=parsl_structure_relaxation.result()[1] , zero_point_energy_id0=zero_point_energy_id0, total_entropy_term_id0=total_entropy_term_id0, energy_minimum_id0=energy_minimum_id0) 
    # Call subroutine #1 from routine #3
    
    parsl_vibrations = vibrations(parameters_id3, parsl_structure_relaxation_id2.result()[0]) 
    # Call subroutine #1 from routine #6
    
    parsl_vibrations_id6 = vibrations(parameters_id6, parsl_structure_relaxation_id5.result()[0]) 
    # Call subroutine #1 from routine #8
    
    parsl_vibrations_id8 = vibrations(parameters_id6, parsl_structure_relaxation_id5.result()[0]) 
    # Call subroutine #1 from routine #9
    
    parsl_vibrations_id9 = vibrations(parameters_id6, parsl_structure_relaxation_id5.result()[0]) 
    # Call subroutine #1 from routine #10
    
    parsl_vibrations_id10 = vibrations(parameters_id6, parsl_structure_relaxation_id5.result()[0]) 
    # Call subroutine #1 from routine #11
    
    parsl_vibrations_id11 = vibrations(parameters_id6, parsl_structure_relaxation_id5.result()[0]) 
    # Call subroutine #1 from routine #12
    
    parsl_vibrations_id12 = vibrations(parameters_id6, parsl_structure_relaxation_id5.result()[0]) 
    # Call subroutine #1 from routine #13
    
    parsl_vibrations_id13 = vibrations(parameters_id6, parsl_structure_relaxation_id5.result()[0]) 

    #### Start Step #4
    # Call subroutine #2 from routine #3
    
    parsl_star_o_id3 = MERGE(energy_minimum_id3=parsl_vibrations.result()[6] , energy_id2=parsl_structure_relaxation_id2.result()[1], zero_point_energy_id3=parsl_vibrations.result()[4] , vibrational_entropy_term_id3=parsl_vibrations.result()[2] , rotational_entropy_term_id3=rotational_entropy_term_id3, translational_entropy_term_id3=translational_entropy_term_id3) 
    # Call subroutine #2 from routine #6
    
    parsl_star_id6 = MERGE(energy_minimum_id6=parsl_vibrations_id6.result()[6] , energy_id5=parsl_structure_relaxation_id5.result()[1], zero_point_energy_id6=parsl_vibrations_id6.result()[4] , vibrational_entropy_term_id6=parsl_vibrations_id6.result()[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6) 
    # Call subroutine #2 from routine #8
    
    parsl_star_id6 = MERGE(energy_minimum_id6=parsl_vibrations_id8.result()[6] , energy_id5=parsl_structure_relaxation_id5.result()[1], zero_point_energy_id6=parsl_vibrations_id8.result()[4] , vibrational_entropy_term_id6=parsl_vibrations_id8.result()[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6) 
    # Call subroutine #2 from routine #9
    
    parsl_star_id6 = MERGE(energy_minimum_id6=parsl_vibrations_id9.result()[6] , energy_id5=parsl_structure_relaxation_id5.result()[1], zero_point_energy_id6=parsl_vibrations_id9.result()[4] , vibrational_entropy_term_id6=parsl_vibrations_id9.result()[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6) 
    # Call subroutine #2 from routine #10
    
    parsl_star_id6 = MERGE(energy_minimum_id6=parsl_vibrations_id10.result()[6] , energy_id5=parsl_structure_relaxation_id5.result()[1], zero_point_energy_id6=parsl_vibrations_id10.result()[4] , vibrational_entropy_term_id6=parsl_vibrations_id10.result()[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6) 
    # Call subroutine #2 from routine #11
    
    parsl_star_id6 = MERGE(energy_minimum_id6=parsl_vibrations_id11.result()[6] , energy_id5=parsl_structure_relaxation_id5.result()[1], zero_point_energy_id6=parsl_vibrations_id11.result()[4] , vibrational_entropy_term_id6=parsl_vibrations_id11.result()[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6) 
    # Call subroutine #2 from routine #12
    
    parsl_star_id6 = MERGE(energy_minimum_id6=parsl_vibrations_id12.result()[6] , energy_id5=parsl_structure_relaxation_id5.result()[1], zero_point_energy_id6=parsl_vibrations_id12.result()[4] , vibrational_entropy_term_id6=parsl_vibrations_id12.result()[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6) 
    # Call subroutine #2 from routine #13
    
    parsl_star_id6 = MERGE(energy_minimum_id6=parsl_vibrations_id13.result()[6] , energy_id5=parsl_structure_relaxation_id5.result()[1], zero_point_energy_id6=parsl_vibrations_id13.result()[4] , vibrational_entropy_term_id6=parsl_vibrations_id13.result()[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6) 

    #### Start Step #5
    # Call subroutine #1 from routine #7
    
    parsl_properties = MERGE(star_id6=parsl_star_id6, star_o_id3=parsl_star_o_id3, o_two_id0=parsl_o_two_id0 )

    #### Start Step #6
    # Call subroutine #2 from routine #7
    
    parsl_reaction_energies = reaction_energies(parsl_properties, reactions) 
    print(parsl_reaction_energies_id13.compute())
    end_time = time.time()
    total_time = end_time - start_time
    total_memtime = end_memtime - start_time
    print(' The total time is' , round(total_time, 2), 'seconds')
    print(' The total memory load time is' , round(total_memtime, 2), 'seconds')
    print('PARSL based workflow')
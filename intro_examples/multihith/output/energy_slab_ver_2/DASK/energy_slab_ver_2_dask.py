import yaml
import numpy as np
from auxiliary import flat_tuple
from auxiliary import flat_list
from auxiliary import MERGE
import dask 
from dask.distributed import Client
from distributed.client import *
from dask_jobqueue import SLURMCluster
import time
from ueffumax import build_structure_generic
from ueffumax import structure_relaxation
from ueffumax import build_structure
from ueffumax import vibrations
from ueffumax import reaction_energies

if __name__ == '__main__':


    start_time = time.time()
    #cluster = SLURMCluster(cores=40, memory='150GB', walltime='01:50:00', queue='single', processes=4)
    #client = Client(cluster)
    #cluster.scale(1)
    #print(cluster.job_script())
    dask.config.set(scheduler='threads')
    #Read Input #1 from subroutine #1 in routine #0
    yaml_stream = open('parameters_id0.yaml', 'r')
    parameters_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #2 from subroutine #3 in routine #0
    yaml_stream = open('zero_point_energy_id0.yaml', 'r')
    zero_point_energy_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #3 from subroutine #3 in routine #0
    yaml_stream = open('total_entropy_term_id0.yaml', 'r')
    total_entropy_term_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #4 from subroutine #3 in routine #0
    yaml_stream = open('energy_minimum_id0.yaml', 'r')
    energy_minimum_id0 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #1 from subroutine #1 in routine #1
    yaml_stream = open('parameters_id1.yaml', 'r')
    parameters_id1 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #1 from subroutine #1 in routine #2
    yaml_stream = open('parameters_id2.yaml', 'r')
    parameters_id2 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #1 from subroutine #1 in routine #3
    yaml_stream = open('parameters_id3.yaml', 'r')
    parameters_id3 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #5 from subroutine #2 in routine #3
    yaml_stream = open('rotational_entropy_term_id3.yaml', 'r')
    rotational_entropy_term_id3 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #6 from subroutine #2 in routine #3
    yaml_stream = open('translational_entropy_term_id3.yaml', 'r')
    translational_entropy_term_id3 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #1 from subroutine #1 in routine #4
    yaml_stream = open('parameters_id4.yaml', 'r')
    parameters_id4 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #1 from subroutine #1 in routine #5
    yaml_stream = open('parameters_id5.yaml', 'r')
    parameters_id5 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #1 from subroutine #1 in routine #6
    yaml_stream = open('parameters_id6.yaml', 'r')
    parameters_id6 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #5 from subroutine #2 in routine #6
    yaml_stream = open('rotational_entropy_term_id6.yaml', 'r')
    rotational_entropy_term_id6 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #6 from subroutine #2 in routine #6
    yaml_stream = open('translational_entropy_term_id6.yaml', 'r')
    translational_entropy_term_id6 = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()
    #Read Input #2 from subroutine #2 in routine #7
    yaml_stream = open('reactions.yaml', 'r')
    reactions = yaml.load(yaml_stream, Loader=yaml.Loader)


    end_memtime = time.time()

    #### Start Step #1
    # Call subroutine #1 from routine #0
    
    lazy_build_structure_generic = dask.delayed(nout=1)(build_structure_generic)(parameters_id0)
    # Call subroutine #1 from routine #1
    
    lazy_build_structure = dask.delayed(nout=1)(build_structure)(parameters_id1)
    # Call subroutine #1 from routine #4
    
    lazy_build_structure_id4 = dask.delayed(nout=1)(build_structure)(parameters_id4)

    #### Start Step #2
    # Call subroutine #2 from routine #0
    
    lazy_structure_relaxation = dask.delayed(nout=4)(structure_relaxation)(parameters_id0, lazy_build_structure_generic )
    # Call subroutine #1 from routine #2
    
    lazy_structure_relaxation_id2 = dask.delayed(nout=4)(structure_relaxation)(parameters_id2, lazy_build_structure)
    # Call subroutine #1 from routine #5
    
    lazy_structure_relaxation_id5 = dask.delayed(nout=4)(structure_relaxation)(parameters_id5, lazy_build_structure_id4)

    #### Start Step #3
    # Call subroutine #3 from routine #0
    
    lazy_o_two_id0 = dask.delayed(MERGE)(energy_id0=lazy_structure_relaxation[1] , zero_point_energy_id0=zero_point_energy_id0, total_entropy_term_id0=total_entropy_term_id0, energy_minimum_id0=energy_minimum_id0)
    # Call subroutine #1 from routine #3
    
    lazy_vibrations = dask.delayed(nout=7)(vibrations)(parameters_id3, lazy_structure_relaxation_id2[0] )
    # Call subroutine #1 from routine #6
    
    lazy_vibrations_id6 = dask.delayed(nout=7)(vibrations)(parameters_id6, lazy_structure_relaxation_id5[0] )
    # Call subroutine #1 from routine #8
    
    lazy_vibrations_id8 = dask.delayed(nout=7)(vibrations)(parameters_id6, lazy_structure_relaxation_id5[0] )
    # Call subroutine #1 from routine #9
    
    lazy_vibrations_id9 = dask.delayed(nout=7)(vibrations)(parameters_id6, lazy_structure_relaxation_id5[0] )
    # Call subroutine #1 from routine #10
    
    lazy_vibrations_id10 = dask.delayed(nout=7)(vibrations)(parameters_id6, lazy_structure_relaxation_id5[0] )
    # Call subroutine #1 from routine #11
    
    lazy_vibrations_id11 = dask.delayed(nout=7)(vibrations)(parameters_id6, lazy_structure_relaxation_id5[0] )
    # Call subroutine #1 from routine #12
    
    lazy_vibrations_id12 = dask.delayed(nout=7)(vibrations)(parameters_id6, lazy_structure_relaxation_id5[0] )
    # Call subroutine #1 from routine #13
    
    lazy_vibrations_id13 = dask.delayed(nout=7)(vibrations)(parameters_id6, lazy_structure_relaxation_id5[0] )

    #### Start Step #4
    # Call subroutine #2 from routine #3
    
    lazy_star_o_id3 = dask.delayed(MERGE)(energy_minimum_id3=lazy_vibrations[6] , energy_id2=lazy_structure_relaxation_id2[1],zero_point_energy_id3=lazy_vibrations[4] , vibrational_entropy_term_id3=lazy_vibrations[2] , rotational_entropy_term_id3=rotational_entropy_term_id3, translational_entropy_term_id3=translational_entropy_term_id3)
    # Call subroutine #2 from routine #6
    
    lazy_star_id6 = dask.delayed(MERGE)(energy_minimum_id6=lazy_vibrations_id6[6] , energy_id5=lazy_structure_relaxation_id5[1],zero_point_energy_id6=lazy_vibrations_id6[4] , vibrational_entropy_term_id6=lazy_vibrations_id6[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6)
    # Call subroutine #2 from routine #8
    
    lazy_star_id6 = dask.delayed(MERGE)(energy_minimum_id6=lazy_vibrations_id8[6] , energy_id5=lazy_structure_relaxation_id5[1],zero_point_energy_id6=lazy_vibrations_id8[4] , vibrational_entropy_term_id6=lazy_vibrations_id8[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6)
    # Call subroutine #2 from routine #9
    
    lazy_star_id6 = dask.delayed(MERGE)(energy_minimum_id6=lazy_vibrations_id9[6] , energy_id5=lazy_structure_relaxation_id5[1],zero_point_energy_id6=lazy_vibrations_id9[4] , vibrational_entropy_term_id6=lazy_vibrations_id9[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6)
    # Call subroutine #2 from routine #10
    
    lazy_star_id6 = dask.delayed(MERGE)(energy_minimum_id6=lazy_vibrations_id10[6] , energy_id5=lazy_structure_relaxation_id5[1],zero_point_energy_id6=lazy_vibrations_id10[4] , vibrational_entropy_term_id6=lazy_vibrations_id10[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6)
    # Call subroutine #2 from routine #11
    
    lazy_star_id6 = dask.delayed(MERGE)(energy_minimum_id6=lazy_vibrations_id11[6] , energy_id5=lazy_structure_relaxation_id5[1],zero_point_energy_id6=lazy_vibrations_id11[4] , vibrational_entropy_term_id6=lazy_vibrations_id11[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6)
    # Call subroutine #2 from routine #12
    
    lazy_star_id6 = dask.delayed(MERGE)(energy_minimum_id6=lazy_vibrations_id12[6] , energy_id5=lazy_structure_relaxation_id5[1],zero_point_energy_id6=lazy_vibrations_id12[4] , vibrational_entropy_term_id6=lazy_vibrations_id12[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6)
    # Call subroutine #2 from routine #13
    
    lazy_star_id6 = dask.delayed(MERGE)(energy_minimum_id6=lazy_vibrations_id13[6] , energy_id5=lazy_structure_relaxation_id5[1],zero_point_energy_id6=lazy_vibrations_id13[4] , vibrational_entropy_term_id6=lazy_vibrations_id13[2] , rotational_entropy_term_id6=rotational_entropy_term_id6, translational_entropy_term_id6=translational_entropy_term_id6)

    #### Start Step #5
    # Call subroutine #1 from routine #7
    
    lazy_properties = dask.delayed(MERGE)(star_id6=lazy_star_id6, star_o_id3=lazy_star_o_id3, o_two_id0=lazy_o_two_id0 )

    #### Start Step #6
    # Call subroutine #2 from routine #7
    
    lazy_reaction_energies = dask.delayed(nout=1)(reaction_energies)(lazy_properties, reactions)
    #task_graph = Prefered_task.visualize(filename = 'mywf_test.png')
    print(lazy_reaction_energies_id13.compute())
    end_time = time.time()
    total_time = end_time - start_time
    total_memtime = end_memtime - start_time
    print(' The total time is' , round(total_time, 2), 'seconds')
    print(' The total memory load time is' , round(total_memtime, 2), 'seconds')
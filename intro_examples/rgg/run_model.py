import os
import argparse
import time

parser = argparse.ArgumentParser()
parser.add_argument(
    '--pool',
    help='Specify pool type. Possible options are [local_threads, slurm] ')
parser.add_argument(
    '--sleep',
    help='Sleep duration(sec) of each task, valid only for balanced case')
parser.add_argument('--scale', help='Specify number of nodes')
parser.add_argument(
    '--worker_per_node',
    help='Specify number of workers per node')
parser.add_argument(
    '--cpu_per_node',
    help='Specify number of cores per worker')
parser.add_argument('--model_path', help='Path to python executable')
parser.add_argument('--maximum_threads', help='Maximum local thread size')



args = parser.parse_args()

if __name__ == '__main__':

    file = args.model_path
    scale = args.scale
    worker_per_node = args.worker_per_node
    cpu_per_node = args.cpu_per_node
    maximum_threads = args.maximum_threads

    if args.pool == 'slurm':
        cmd = "python " + str(file) + " --sleep " + str(args.sleep) + " --cpu_per_node " + str(cpu_per_node) + " --scale " + str(
            scale) + " --worker_per_node " + str(worker_per_node) + " --pool " + str(args.pool)
        os.system(cmd)
    elif args.pool == 'local_threads':
        cmd = "python " + str(file) + " --sleep " + \
            str(args.sleep) + " --maximum_threads " + str(maximum_threads) + " --pool " + str(args.pool)
        os.system(cmd)
    else:
        print("The selected pool type does not exist")

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=76
#SBATCH --time=23:00:00
#SBATCH --mem=5000   
#SBATCH --export=ALL
#SBATCH --output="10000nodes_fireworks_1sec_%j.out"

#Usually you should set
export KMP_AFFINITY=compact,1,0
printenv | grep SLURM
#export KMP_AFFINITY=verbose,compact,1,0 prints messages concerning the supported affinity
#KMP_AFFINITY Description: https://software.intel.com/en-us/node/524790#KMP_AFFINITY_ENVIRONMENT_VARIABLE
source /home/hk-project-test-sdlmat/th7356/work/gitlab/wfgenes/wfGenes_exe/setenv_cluster.sh
#echo "Running pywrapper ruscript"
startexe="python run_pywrapper.py --sleep $1 --wms $2"
echo $startexe
exec $startexe

